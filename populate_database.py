from bson.objectid import ObjectId

import crawler
from mongodb.database import get_database


regions = [
    {"region_int_id": 1, "name": "Г.АСТАНА"},
    {"region_int_id": 2, "name": "Г.АЛМАТЫ"},
    {"region_int_id": 3, "name": "АКМОЛИНСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 4, "name": "АКТЮБИНСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 5, "name": "АЛМАТИНСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 6, "name": "АТЫРАУСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 7, "name": "ЗАПАДНО-КАЗАХСТАНСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 8, "name": "ЖАМБЫЛСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 9, "name": "КАРАГАНДИНСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 10, "name": "КОСТАНАЙСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 11, "name": "КЫЗЫЛОРДИНСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 12, "name": "МАНГИСТАУСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 13, "name": "ТУРКЕСТАНСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 14, "name": "ПАВЛОДАРСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 15, "name": "СЕВЕРО-КАЗАХСТАНСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 16, "name": "ВОСТОЧНО-КАЗАХСТАНСКАЯ ОБЛАСТЬ"},
    {"region_int_id": 17, "name": "Г.ШЫМКЕНТ"},
    {"region_int_id": 18, "name": "ОБЛАСТЬ АБАЙ"},
    {"region_int_id": 19, "name": "ОБЛАСТЬ ЖЕТІСУ"},
    {"region_int_id": 20, "name": "ОБЛАСТЬ ҰЛЫТАУ"},
]

def populate_database():
    mongo_client = get_database()

    regions_collection = mongo_client["regions"]
    # regions_collection.insert_many(documents=regions)


    crawler_krisha_kz = crawler.KrishaKZCrawler(total_page_num=1)
    offers_documents = crawler_krisha_kz.get_offers()
    for i in range(0, len(offers_documents)):
        if offers_documents[i]["offer"].get('Город') is not None and "Астана" in offers_documents[i]["offer"]["Город"]:
            reg = regions_collection.find_one({"name": "Г.АСТАНА"})
            if reg is not None:
                offers_documents[i]["region_id"] = reg["_id"]
        if offers_documents[i]["offer"].get('Город') is not None and "Алматы" in offers_documents[i]["offer"]["Город"]:
            reg = regions_collection.find_one({"name": "Г.АЛМАТЫ"})
            if reg is not None:
                offers_documents[i]["region_id"] = reg["_id"]

        if offers_documents[i]["data"]["hasPrice"] is True:
            offers_documents[i]["price"] = offers_documents[i]["data"]["price"]
        if offers_documents[i]["data"]["title"] is not None:
            offers_documents[i]["title"] = offers_documents[i]["data"]["title"]
        if offers_documents[i]["data"]["addressTitle"] is not None:
            offers_documents[i]["addressTitle"] = offers_documents[i]["data"]["addressTitle"]
        if offers_documents[i]["data"]["square"] is not None:
            offers_documents[i]["square"] = offers_documents[i]["data"]["square"]
        if offers_documents[i]["data"]["rooms"] is not None:
            offers_documents[i]["rooms"] = offers_documents[i]["data"]["rooms"]
        del offers_documents[i]["data"]

    offers = mongo_client["offers"]

    offers.insert_many(documents=offers_documents)
    print(len(offers_documents))


if __name__ == "__main__":
    populate_database()
