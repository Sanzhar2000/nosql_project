from fastapi import FastAPI
import uvicorn

from routers import router as CRUDRouter

from config import settings


app: FastAPI = FastAPI(root_path=settings.ROOT_PATH)
app.include_router(router=CRUDRouter)


@app.get("/api/healthchecker")
def root():
    return {"message": "Welcome to FastAPI with MongoDB"}


if __name__ == '__main__':
    uvicorn.run(app=app, host="0.0.0.0", port=8003)
