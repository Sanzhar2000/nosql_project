import json
import random
import time

import requests
from bs4 import BeautifulSoup


class KrishaKZCrawler:
    def __init__(self, total_page_num: int):
        self.session: requests.Session = requests.Session()
        self.total_page_num: int = total_page_num
        self.offers = []
        self.custom_ids = [
            #  Astana
            683246175,
            683066926,
            #  Almaty
            682937670,
            681292307
        ]

    def get_links(self):
        links = []
        for i in range(1, self.total_page_num + 1):
            links_from_one_page = self.get_links_from_one_page(url=f"https://krisha.kz/prodazha/kvartiry/?das[house.year][to]=2022&page={i}")
            print(len(links_from_one_page))
            links.extend(links_from_one_page)
            time.sleep(random.randint(2, 5))
        return links

    def get_links_from_one_page(self, url):
        response = self.session.get(url=url)
        soup = BeautifulSoup(markup=response.text, features="html.parser")
        links = []
        for i in soup.find_all("a", {"class": "a-card__title"}):
            links.append(i.get("href"))
        return links

    def get_offers(self):
        for link in self.get_links():
            offer = self.get_info(url=f"https://krisha.kz{link}")
            offer["offer_source_id"] = int(link.split("/")[-1])
            self.offers.append(offer)

        print(len(self.offers))
        print(self.offers)
        return self.offers

    def get_info(self, url):
        response = self.session.get(url=url)
        if response.status_code != 200:
            d = {
                "error": response.status_code
            }
            return d
        soup = BeautifulSoup(markup=response.text, features="html.parser")
        offerShort = soup.find_all("div", {"offer__short-description"})

        # get offer_info
        titles = []
        for i in offerShort[0].find_all("div", {"class": "offer__info-title"}):
            titles.append(i.get_text())

        values = []
        for i in offerShort[0].find_all("div", {"class": "offer__advert-short-info"}):
            values.append(" ".join(i.get_text().split()))

        d = dict(zip(titles, values))

        # get parameters

        offerParams = soup.find("div", {"offer__parameters"})
        paramKeys = []
        for i in offerParams.find_all("dt"):
            paramKeys.append(i.get_text())
        values = []
        for i in offerParams.find_all("dd"):
            values.append(i.get_text())

        d1 = dict(zip(paramKeys, values))

        # get jsdata
        js = soup.find("script", {"id": "jsdata"})
        txt = js.get_text()
        d2 = json.loads(txt[txt.find("{"):txt.rfind("}") + 1])
        d2 = d2["advert"]
        d2.pop("photos")
        item = {}
        item["offer"] = d
        item["params"] = d1
        item["data"] = d2

        return item


if __name__ == "__main__":
    krisha_kz_crawler = KrishaKZCrawler(total_page_num=2)
    print(krisha_kz_crawler.get_offers())
