from fastapi import APIRouter, Request

from mongodb import crud


router = APIRouter(prefix="/crud")


@router.get("/read_all")
def read_all():
    return crud.get_all()


@router.get("/read_one")
def read_one(id: str):
    return crud.get_one(id=id)


@router.post("/create")
async def create_one(request: Request):
    obj = await request.json()
    return crud.create_one(obj)


@router.put("/update")
async def update_one(id: str, request: Request):
    obj = await request.json()
    return crud.update_one(id=id, obj=obj)


@router.delete("/delete")
def delete_one(id: str):
    return crud.delete_one(id=id)
