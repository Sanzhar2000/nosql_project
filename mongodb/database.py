from pymongo import MongoClient, ASCENDING

from config import settings


def get_database():
    connection_url = f"mongodb+srv://{settings.MONGO_USERNAME}:{settings.MONGO_PASSWORD}@cluster0.skrbn4l.mongodb.net/?retryWrites=true&w=majority"

    client = MongoClient(connection_url)
    db = client["krisha"]

    Region = db.regions
    Region.create_index([("region_int_id", ASCENDING)], unique=True)

    Offer = db.offers
    Offer.create_index([("offer_source_id", ASCENDING)], unique=True)
    return db


if __name__ == "__main__":
    db = get_database()
    print(db)
