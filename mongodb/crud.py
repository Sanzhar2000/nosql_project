from .database import get_database

from bson.objectid import ObjectId


mongo_client = get_database()
offers_collection = mongo_client["offers"]
regions_collection = mongo_client["regions"]


def get_region(id: ObjectId):
    res = regions_collection.find_one({"_id": id})
    if res is not None:
        res["_id"] = str(res["_id"])
    return res


def get_all():
    offers = []
    for item in offers_collection.find():
        item["_id"] = str(item["_id"])
        if item.get("region_id"):
            item["region"] = get_region(id=item.get("region_id"))
            del item["region_id"]
        offers.append(item)
    print(offers)
    return offers


def get_one(id: str):
    item = offers_collection.find_one({"_id": ObjectId(id)})
    if item is not None:
        item["_id"] = str(item["_id"])
        if item.get("region_id"):
            item["region"] = get_region(id=item.get("region_id"))
            del item["region_id"]
    return item


def create_one(obj: dict):
    return offers_collection.insert_one(document=obj)


def update_one(id: str, obj):
    record = get_one(id=id)
    if obj.get("region") is not None:
        obj["region"]["_id"] = ObjectId(obj["region"]["_id"])
    if record is not None:
        offers_collection.replace_one(filter={"_id": ObjectId(id)}, replacement=obj)
        return True


def delete_one(id: str):
    item = offers_collection.delete_one({"_id": ObjectId(id)})
    return item.deleted_count


if __name__ == "__main__":
    r = get_one(id="644fd5a8740b30b1b15d2c15")
    print(r)
