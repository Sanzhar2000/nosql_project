import os

from dotenv import load_dotenv, find_dotenv


load_dotenv(dotenv_path=find_dotenv())


class Settings:
    PROJECT_NAME: str = "..."
    PROJECT_VERSION: str = "1.0.0"
    PROJECT_DESCRIPTION: str = "..."
    ROOT_PATH: str = os.getenv("ROOT_PATH", "")

    APP_ENV: str = os.getenv("APP_ENV", "production")
    APP_DEBUG: bool = os.getenv("APP_DEBUG", False)
    APP_LOG_LEVEL: str = os.getenv("APP_LOG_LEVEL", "info")

    MONGO_USERNAME: str = os.getenv(key="MONGO_USERNAME")
    MONGO_PASSWORD: str = os.getenv(key="MONGO_PASSWORD")


settings = Settings()
